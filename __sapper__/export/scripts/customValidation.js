﻿/// <reference path="jquery.validate-vsdoc.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
  
jquery.validator.addMethod("greater", function(value, element, param) {
    return Date.parse(value) > Date.parse($(param).val());
});

jquery.validator.unobtrusive.adapters.add("greater", ["other"], function(options) {
    options.rules["greater"] = "#" + options.params.other;
    options.messages["greater"] = options.message;
})